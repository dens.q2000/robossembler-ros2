#include "config/options.hpp"

#include "glog/logging.h"

namespace env_manager
{
namespace config
{

ComponentOption ComponentOption::create_option(
    LuaParameterDictionary *lua_parameter_dictionary)
{
    return ComponentOption{
        lua_parameter_dictionary->GetString("lib"),
        lua_parameter_dictionary->GetString("class"),
    };
}

EnvironmentOption EnvironmentOption::create_option(
    LuaParameterDictionary *lua_parameter_dictionary)
{
    auto comps = 
        lua_parameter_dictionary->GetDictionary("components");
    std::map<std::string, ComponentOption> components;
    for (const auto &comp: comps->GetKeys())
        components.insert({
            comp, ComponentOption::create_option(comps->GetDictionary(comp).get())
        });
    
    return EnvironmentOption{
        lua_parameter_dictionary->GetString("namespace"),
        components,
    };
}

NodeOption NodeOption::create_option(
    LuaParameterDictionary *lua_parameter_dictionary)
{    
    return NodeOption{
        lua_parameter_dictionary->GetString("name"),
        lua_parameter_dictionary->GetString("msg_type"),
        node_type_remap.at(lua_parameter_dictionary->GetString("type")),
    };
}

const std::unordered_map<std::string, NodeOption::NodeType> 
NodeOption::node_type_remap = 
    {
        {"Publisher", NodeOption::NodeType::Publisher},
        {"Subscriber", NodeOption::NodeType::Subscriber},
        {"Service", NodeOption::NodeType::Service},
        {"Client", NodeOption::NodeType::Client},
    };

EnvManagerOption EnvManagerOption::create_option(
    LuaParameterDictionary *lua_parameter_dictionary)
{
    
    auto lua_env_dictionary = lua_parameter_dictionary->GetDictionary("environments");
    auto envs_dict = lua_env_dictionary->GetArrayValuesAsDictionaries();
    std::map<std::string, EnvironmentOption> environments;
    for (const auto& env: envs_dict)
        environments.insert({
            env->GetString("namespace"), EnvironmentOption::create_option(env.get())
        });

    auto lua_node_dictionary = lua_parameter_dictionary->GetDictionary("nodes");
    auto nodes_keys = lua_node_dictionary->GetKeys();
    std::map<std::string, NodeOption> nodes;
    for (const auto& node: nodes_keys)
        nodes.insert({
            node, NodeOption::create_option(lua_node_dictionary->GetDictionary(node).get())
        });

    return EnvManagerOption{
        environments,
        nodes,
    };
}

} // namespace config
} // namespace env_manager
