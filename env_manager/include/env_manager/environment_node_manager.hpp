#ifndef ENV_MANAGER__ENVIRONMENT_NODE_MANAGER_HPP_
#define ENV_MANAGER__ENVIRONMENT_NODE_MANAGER_HPP_

#include "config/options.hpp"

#include <vector>

#include <rclcpp/executors.hpp>
#include <boost/thread.hpp>

extern "C"
{
#include <rcl/context.h>
#include <rcl/node.h>
}

namespace env_manager
{

class EnvironmentNodeManager
{
public:
    EnvironmentNodeManager();
    
    ~EnvironmentNodeManager();
};

} // namespace env_manager

#endif // ENV_MANAGER__ENVIRONMENT_NODE_MANAGER_HPP_
