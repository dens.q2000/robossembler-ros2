#ifndef ENV_MANAGER_CONFIG_CONFIG_H_
#define ENV_MANAGER_CONFIG_CONFIG_H_

namespace env_manager 
{
namespace config 
{

constexpr char kConfigurationFilesDirectory[] = "@ENV_MANAGER_CONFIGURATION_FILES_DIRECTORY@";
constexpr char kSourceDirectory[] = "@PROJECT_SOURCE_DIR@";

}  // namespace config
}  // namespace env_manager

#endif  // ENV_MANAGER_CONFIG_CONFIG_H_
