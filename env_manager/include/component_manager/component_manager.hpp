#ifndef COMPONENT_MANAGER__COMPONENT_MANAGER_HPP_
#define COMPONENT_MANAGER__COMPONENT_MANAGER_HPP_

#include "component_manager/visibility_control.h"
#include "config/options.hpp"

#include <rclcpp/rclcpp.hpp>
#include <rclcpp_components/component_manager.hpp>
#include <rclcpp_components/node_factory.hpp>
#include <class_loader/class_loader.hpp>

#include <glog/logging.h>


namespace env_manager
{
namespace component_manager
{

/**
 * This class implements the system for managing and configuring loaded components. 
 * 
 * It is assumed that the loaded components are inherited from the classes 
 *  provided in the library for each node type. 
 */
class ComponentManager
{
public:
    ComponentManager(std::weak_ptr<rclcpp::Executor> executor);

    void register_components(
            const std::map<std::string, config::ComponentOption> &comps, 
            const std::map<std::string, config::NodeOption> &nodes,
            const std::string& ns);

    void remove_components_from_executor();

    void remap_components_namespace(const std::string& ns);

private:
    std::weak_ptr<rclcpp::Executor> _executor;
    std::vector<class_loader::ClassLoader* > _loaders;
    std::map<std::string, rclcpp_components::NodeInstanceWrapper> _node_wrappers;
    std::map<std::string, rclcpp::node_interfaces::NodeBaseInterface::SharedPtr> _nodes;
};

} // namespace env_manager
} // namespace component_manager

#endif  // COMPONENT_MANAGER__COMPONENT_MANAGER_HPP_
