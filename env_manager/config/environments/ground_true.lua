-- environment configuraiton
GROUND_TRUE = {
    namespace = "ground_true",
    components = {
        talker_node = {
            lib = "libpub_component.so",
            class = "pub_component::Publisher",
        },
        service_node = {
            lib = "libsrv_component.so",
            class = "srv_component::Service"
        }
    }
}
