from launch import LaunchDescription
from launch.actions import (
    DeclareLaunchArgument,
    IncludeLaunchDescription,
    OpaqueFunction,
    RegisterEventHandler,
)
from launch.conditions import IfCondition, UnlessCondition
from launch.event_handlers import OnProcessExit
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.substitutions import Command, FindExecutable, LaunchConfiguration, PathJoinSubstitution
from launch_ros.actions import Node
from launch_ros.substitutions import FindPackageShare
import xacro
import os
from ament_index_python import get_package_share_directory


def generate_launch_description():

    launch_args = []
    launch_args.append(DeclareLaunchArgument(
            name="robot_name",
            default_value="rasmt",
            description="Set robot name in gazebo env"
        )
    )
    launch_args.append(DeclareLaunchArgument(
            name="robot_description_content",
            description="Robot XML file"
        )
    )

    #robot_description_content = {"robot_description":LaunchConfiguration("robot_description_content")}
    robot_description_content = LaunchConfiguration("robot_description_content")
    # launch Ignition Gazebo
    pkg_ros_ign_gazebo = get_package_share_directory('ros_ign_gazebo')
    gazebo = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(
            os.path.join(pkg_ros_ign_gazebo, 'launch', 'ign_gazebo.launch.py')),
        launch_arguments={'ign_args': '-r empty.sdf'}.items(),
    )
    
    ros2_ign_bridge = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(
            PathJoinSubstitution(
                [
                    FindPackageShare("rasmt_support"),
                    "launch",
                    "rasmt_ignition_bridge.launch.py"
                ]
            )
        )
    )

   # Spawn
    spawn = Node(package='ros_ign_gazebo', executable='create',
                 arguments=[
                    '-string', robot_description_content,
                    '-name', 'rasmt',
                    '-allow_renaming', 'true'],
                 output='screen')


    launch_nodes = []
    launch_nodes.append(gazebo)
    launch_nodes.append(spawn)
    launch_nodes.append(ros2_ign_bridge)


    return LaunchDescription(launch_args + launch_nodes)