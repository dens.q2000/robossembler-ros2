# Robossembler package for description of robot2

This package consists of files describing the AQUA model of the robot, the entire scene and the gripper device. It also contains startup files for launching robot controllers and simulations in gazebo.

```
.
├── CMakeLists.txt
├── config
│   ├── rasmt_effort_controllers.yaml             # File describing the robot controllers
│   └── rasmt.rviz
├── launch
│   ├── rasmt_control.launch.py                 # Launch file for running robot controlles
│   ├── rasmt_gazebo.launch.py                  # File for running gazebo simulation
│   └── rviz.launch.py                          # File for running RViz visualisation
├── meshes
│   ├── collision
│   │   ├── Base_Link.stl
│   │   ├── cube5x.stl
│   │   ├── Dock_Link.stl
│   │   ├── Fork_1.stl
│   │   ├── Fork_2.stl
│   │   ├── Fork_3.stl
│   │   ├── Grip_Body.stl
│   │   ├── Grip_L.stl
│   │   ├── Grip_R.stl
│   │   ├── Link_1.stl
│   │   └── Link_2.stl
│   └── visual
├── package.xml
├── README.md
└── urdf
    ├── cube5x.sdf                              # The cube inside which the robot works
    ├── rasmt.xacro                             # Main Robot scene description file
    ├── robot                                   # Single robot description files
    │   ├── rasmt_single_control.xacro
    │   ├── rasmt_single_gazebo.xacro
    │   └── rasmt_single_macro.xacro
    └── tools                                   # Robot tool description files
        ├── rasmt_hand_control.xacro
        ├── rasmt_hand_gazebo.xacro
        └── rasmt_hand_macro.xacro
```