from launch import LaunchDescription
from launch_ros.actions import Node
from ament_index_python.packages import get_package_share_directory 
import os


def generate_launch_description():

    bt_config = os.path.join(
        get_package_share_directory('rbs_bt_executor'),
        'config',
        'params.yaml'
    )
    points = os.path.join(
        get_package_share_directory('rbs_bt_executor'),
        'config',
        'points.yaml'
    )


    return LaunchDescription([
        Node(
            package='behavior_tree',
            namespace='',
            executable='bt_engine',
            name="bt_engine",
            # Do not declare a node name otherwise it messes with the action node names and will result in
            # duplicate nodes!
            parameters=[
                {'bt_file_path': os.path.join(get_package_share_directory('rbs_bt_executor'), 'bt_trees/test_tree.xml')},
                {'plugins': ['rbs_skill_move_topose_bt_action_client']},
                points
            ]
        ),
    ])
