#include "rbs_skill_interfaces/action/moveit_send_pose.hpp"

#include "behaviortree_cpp_v3/bt_factory.h"
#include "behavior_tree/BtAction.hpp"
#include "geometry_msgs/msg/pose_stamped.hpp"
#include "moveit_msgs/action/move_group.h"
// #include "Eigen/Geometry"
#include "rclcpp/parameter.hpp"

using namespace BT;
using MoveToPoseAction = rbs_skill_interfaces::action::MoveitSendPose;
static const rclcpp::Logger LOGGER = rclcpp::get_logger("MoveToPoseActionClient");

class MoveToPose : public BtAction<MoveToPoseAction>
{
    public:
        MoveToPose(const std::string & name, const BT::NodeConfiguration & config) 
        : BtAction<MoveToPoseAction>(name, config) 
        {
            if (!_node->has_parameter("pose1"))
            {
                _node->declare_parameter("pose1",std::vector<double>{   0.11724797630931184,
                                                                        0.46766635768602544,
                                                                        0.5793862343094913,
                                                                        0.9987999001314066,
                                                                        0.041553846820940925,
                                                                        -0.004693314677006583,
                                                                        -0.025495295825239704
                                                                    }
                                        );
            }

            if (!_node->has_parameter("pose2"))
            {
                _node->declare_parameter("pose2",std::vector<double>{   -0.11661364861606047,
                                                                        0.4492600889665261,
                                                                        0.5591700913807053,
                                                                        0.9962273179258467,
                                                                        0.04057380155886888,
                                                                        0.009225849745372298,
                                                                        0.07615629548377048
                                                                    }
                                        );
            }

            if (!_node->has_parameter("pose3"))
            {
                _node->declare_parameter("pose3",std::vector<double>{   -0.07133612047767886,
                                                                        0.41038906578748613,
                                                                        0.2844649846989331,
                                                                        0.999078481789772,
                                                                        0.04109234110437432,
                                                                        0.006539754292177074,
                                                                        0.010527978961032304
                                                                    }
                                        );
            }
            RCLCPP_INFO(_node->get_logger(), "Start the node");
        }

        MoveToPoseAction::Goal populate_goal() override
        {
            getInput<std::string>("robot_name", robot_name_);
            getInput<std::string>("pose", pose_);
            
            rclcpp::Parameter arr = _node->get_parameter(pose_);
            std::vector<double> possd = arr.as_double_array();

            pose_des.position.x = possd[0];
            pose_des.position.y = possd[1];
            pose_des.position.z = possd[2];
            pose_des.orientation.x = possd[3];
            pose_des.orientation.y = possd[4];
            pose_des.orientation.z = possd[5];
            pose_des.orientation.w = possd[6];
            

            RCLCPP_INFO(_node->get_logger(), "\n Send Pose: \n\t pose.x: %f \n\t pose.y: %f \n\t pose.z: %f \n\n\t opientation.x: %f \n\t orientation.y: %f \n\t orientation.z: %f \n\t orientation.w: %f", 
               pose_des.position.x, pose_des.position.y, pose_des.position.z,
               pose_des.orientation.x, pose_des.orientation.y, pose_des.orientation.z, pose_des.orientation.w);

            auto goal = MoveToPoseAction::Goal();
            RCLCPP_INFO(_node->get_logger(), "Send goal to robot [%s]", robot_name_.c_str());
            
            goal.robot_name = robot_name_;
            goal.target_pose = pose_des;
            goal.end_effector_acceleration = 1.0;
            goal.end_effector_velocity = 1.0;
            
            RCLCPP_INFO(_node->get_logger(), "Goal populated");

            return goal;
        }

        static PortsList providedPorts()
        {
            return providedBasicPorts({
                InputPort<std::string>("robot_name"),
                InputPort<std::string>("pose")
            });
        }

    private:
        std::string robot_name_;
        std::string pose_;
        geometry_msgs::msg::Pose pose_des;
        std::map<std::string, geometry_msgs::msg::Pose> Poses;

        // geometry_msgs::msg::Pose array_to_pose(std::vector<double> pose_array){

        // }


}; // class MoveToPose

BT_REGISTER_NODES(factory)
{
    factory.registerNodeType<MoveToPose>("MoveToPose");
}