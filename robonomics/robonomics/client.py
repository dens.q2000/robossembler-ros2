import sys
import ipfshttpclient as IPFS
import robonomicsinterface as RI
import rclpy

from base58 import b58decode, b58encode
from rclpy.node import Node
from rclpy.action import ActionClient
from plansys2_msgs.srv import GetDomain, GetPlan, AddProblem
from plansys2_msgs.action import ExecutePlan
from action_msgs.msg import GoalStatus

class Planner(Node):

    def __init__(self):
        super().__init__('planner_client_async')
        self.cli = self.create_client(GetPlan, '/planner/get_plan')
        while not self.cli.wait_for_service(timeout_sec=1.0):
            self.get_logger().info('service not available, waiting again...')
        self.req = GetPlan.Request()

    def get_plan(self, domain, problem):
        self.req.domain = domain
        self.req.problem = problem
        self.future = self.cli.call_async(self.req)

class ProblemAdder(Node):

    def __init__(self):
        super().__init__('problem_expert_client_async')
        self.cli = self.create_client(AddProblem, '/problem_expert/add_problem')
        while not self.cli.wait_for_service(timeout_sec=1.0):
            self.get_logger().info('service not available, waiting again...')
        self.req = AddProblem.Request()

    def add_problem(self, problem):
        self.req.problem = problem
        self.future = self.cli.call_async(self.req)


class Executor(Node):

    def __init__(self):
        super().__init__('execute_plan_action_client')
        self._action_client = ActionClient(self, ExecutePlan, 'execute_plan')

    def goal_response_callback(self, future):
        goal_handle = future.result()
        if not goal_handle.accepted:
            self.get_logger().info('Goal rejected :(')
            return

        self._goal_handle = goal_handle

        self.get_logger().info('Goal accepted :)')

        self._get_result_future = goal_handle.get_result_async()
        self._get_result_future.add_done_callback(self.get_result_callback)

    def feedback_callback(self, feedback):
        self.get_logger().info('Received feedback: {0}'.format(feedback.feedback.action_execution_status))

    def get_result_callback(self, future):
        result = future.result().result
        status = future.result().status
        if status == GoalStatus.STATUS_SUCCEEDED:
            self.get_logger().info('Goal succeeded! Result: {0}'.format(result.action_execution_status))
        else:
            self.get_logger().info('Goal failed with status: {0}'.format(status))

        # Shutdown after receiving a result
        rclpy.shutdown()

    def send_goal(self, plan):
        self.get_logger().info('Waiting for action server...')
        self._action_client.wait_for_server()

        goal_msg = ExecutePlan.Goal()
        goal_msg.plan = plan

        self.get_logger().info('Sending goal request...')

        self._send_goal_future = self._action_client.send_goal_async(goal_msg)
            # feedback_callback=self.feedback_callback)
        
        self._send_goal_future.add_done_callback(self.goal_response_callback)


def main(args=None):
    rclpy.init(args=args)
    seed = sys.argv[1]
    robonomics = RI.RobonomicsInterface(seed=seed, remote_ws='ws://127.0.0.1:9944')
    addr = robonomics.define_address()

    def launch_event_execution(data):
        # Fetch IPFS content for task specification
        ipfs = IPFS.connect()
        cid = robonomics.ipfs_32_bytes_to_qm_hash(data[2])
        dir = ipfs.object.get(cid)
        print("Task specification CID: ", cid)
        for link in dir['Links']:
            if link['Name'] == 'domain.pddl':
                domain = ipfs.cat(link['Hash']).decode("utf-8")
                print("Got PDDL domain", domain)
            if link['Name'] == 'problem.pddl':
                problem = ipfs.cat(link['Hash']).decode("utf-8")
                print("Got PDDL problem", problem)
        
        # Init & run ROS2 service client
        planner_client = Planner()
        planner_client.get_plan(domain, problem)
        rclpy.spin_until_future_complete(planner_client, planner_client.future)
        if planner_client.future.done():
            try:
                response = planner_client.future.result()
            except Exception as e:
                planner_client.get_logger().info(
                    'GetPlan call failed %r' % (e,))
            else:
                if response.success:
                    plan = response.plan
                    planner_client.get_logger().info('Got Plan: %s' % (response.plan))
        planner_client.destroy_node()

        problem_client = ProblemAdder()
        problem_client.add_problem(problem)
        problem_client.get_logger().info('Problem Adder Client Started')
        rclpy.spin_until_future_complete(problem_client, problem_client.future)
        if problem_client.future.done():
            problem_client.get_logger().info('Problem client future done')
            try:
                response = problem_client.future.result()
            except Exception as e:
                problem_client.get_logger().info(
                    'AddProblem failed %r' % (e,))
            else:
                if response.success:
                    problem_client.get_logger().info('Problem Added! Starting Plan Execution...')
                    executor_client = Executor()
                    executor_client.send_goal(plan)
                    rclpy.spin(executor_client)
                else:   
                    problem_client.get_logger().info('Problem Add Failed: %s', response.error_info)
        problem_client.destroy_node()

    while rclpy.ok():
        RI.Subscriber(robonomics, RI.SubEvent.NewLaunch, launch_event_execution, addr)


if __name__ == '__main__':
    main()