# Robossembler MoveIt2 package

This package contains the basic robot configuration files for MoveIt2
```
.
├── CMakeLists.txt
├── config
│   ├── cartesian_limits.yaml
│   ├── chomp_planning.yaml
│   ├── joint_limits.yaml
│   ├── kinematics.yaml
│   ├── ompl_planning.yaml
│   ├── rasmt_moveit_controllers.yaml
│   ├── rasmt_moveit.rviz
│   └── rasmt.srdf
├── launch
│   └── rasmt_moveit.launch.py
├── package.xml
└── README.md
```